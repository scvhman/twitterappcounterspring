function main() {
    inputListener();
}

function inputListener() {
    $(document).on("keypress", function (e) {
        if (e.keyCode == 13 && e.shiftKey) {
            if ($("#inputholder").val().length > 0) {
                toastr.info("Performing request", "Hold on a while");
                retrieveHashtagInfo($("#inputholder").val());
            }
        }
    });
}

function retrieveHashtagInfo(text) {
    $.ajax({
        url: "rest?" + $.param({
            hashtag: text
        }, true),
        type: "GET",
        success: function (data) {
            populate(data);
            toastr.success("Success!", "Here's stats");
        },
        error: function (xhr, status) {
            toastr.error("Error", "Something is wrong");
        }
    });
}

function populate(data) {
    $("#outputHolder").html("");
    $.each(data.stamps, function (k, v) {
        addNew(k, v);
    });
}

function addNew(k, v) {
    $("#outputHolder").append("<div class=\"window\">" + k + " : " + v + "</div>");
}
