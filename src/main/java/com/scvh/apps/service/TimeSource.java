package com.scvh.apps.service;

import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

@Component
public class TimeSource {

    private DateTime getCurrentDate() {
        return new DateTime().withTimeAtStartOfDay();
    }

    public long getMinTime() {
        return getCurrentDate().getMillis();
    }

    public String getMinDate() {
        DateTime prev = getCurrentDate().minusDays(2);
        return prev.getYear() + "-" + prev.getMonthOfYear() + "-" + prev.getDayOfMonth();
    }
}
