package com.scvh.apps.frontend.presentors;

import com.scvh.apps.providers.atoms.StructuredResult;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

@Component
public class JsonConverter {

    public Observable<JsonAnswer> convertToJsonAnswer(Observable<StructuredResult> results) {
        return results.subscribeOn(Schedulers.newThread()).map(result -> {
            JsonAnswer answer = new JsonAnswer();
            for (Map.Entry<Interval, List<Tweet>> entry : result.getResult().entrySet()) {
                answer.addToMap(buildString(entry.getKey()), entry.getValue().size());
            }
            return answer;
        });
    }

    private String buildString(Interval interval) {
        StringBuilder buffer = new StringBuilder();
        DateTime start = interval.getStart();
        DateTime end = interval.getEnd();
        buffer.append(start.dayOfMonth().get()).append(".").append(start
                .monthOfYear().get()).append(".").append(start.getYear());
        buffer.append(" ").append(start.hourOfDay().get()).append(":").append("00");
        buffer.append(" - ");
        buffer.append(end.dayOfMonth().get()).append(".").append(end
                .monthOfYear().get()).append(".").append(end.getYear());
        buffer.append(" ").append(end.hourOfDay().get()).append(":").append("00");
        return buffer.toString();
    }
}
