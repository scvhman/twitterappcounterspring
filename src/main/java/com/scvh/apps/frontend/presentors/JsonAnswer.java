package com.scvh.apps.frontend.presentors;

import java.util.HashMap;
import java.util.Map;

public class JsonAnswer {

    private Map<String, Integer> stamps;

    public Map<String, Integer> getStamps() {
        return stamps;
    }

    void addToMap(String time, Integer number) {
        if (stamps == null) {
            stamps = new HashMap<>();
        }
        stamps.put(time, number);
    }
}
