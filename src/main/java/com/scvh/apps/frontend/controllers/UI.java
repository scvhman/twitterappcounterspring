package com.scvh.apps.frontend.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UI {

    @RequestMapping("/")
    public String index() {
        return "index";
    }

}
