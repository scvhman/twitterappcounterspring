package com.scvh.apps.frontend.controllers;

import com.scvh.apps.frontend.presentors.JsonAnswer;
import com.scvh.apps.frontend.presentors.JsonConverter;
import com.scvh.apps.providers.ResultProvider;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
public class Rest {

    private ResultProvider provider;
    private JsonConverter converter;

    @Inject
    public void setProvider(ResultProvider provider) {
        this.provider = provider;
    }

    @Inject
    public void setConverter(JsonConverter converter) {
        this.converter = converter;
    }

    @RequestMapping("/rest")
    public JsonAnswer rest(@RequestParam("hashtag") String hashtag) {
        return converter.convertToJsonAnswer(provider.searchForTag(hashtag)).blockingFirst();
    }
}
