package com.scvh.apps.providers;

import com.scvh.apps.providers.atoms.SearchResult;
import com.scvh.apps.providers.atoms.StructuredResult;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.stereotype.Component;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

@Component
public class ResultConverter {

    Observable<StructuredResult> convertToJsonResponse(Observable<SearchResult> result, long
            minTime) {
        return result.subscribeOn(Schedulers.newThread()).map(tweets -> {
            StructuredResult structuredResult = new StructuredResult();
            tweets.getTweets().stream().filter(tweet -> tweet.getCreatedAt().getTime() < minTime);
            for (Tweet tweet : tweets.getTweets()) {
                DateTime timeFirst = new DateTime(tweet.getCreatedAt().getTime())
                        .withMinuteOfHour(0)
                        .withSecondOfMinute(0);
                DateTime timeSecond = timeFirst.plusHours(1);
                Interval period = new Interval(timeFirst, timeSecond);
                structuredResult.putIntoMap(period, tweet);
            }
            return structuredResult;
        });
    }
}
