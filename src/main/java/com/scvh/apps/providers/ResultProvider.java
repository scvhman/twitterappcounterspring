package com.scvh.apps.providers;

import com.scvh.apps.app.TwitterService;
import com.scvh.apps.providers.atoms.SearchResult;
import com.scvh.apps.providers.atoms.StructuredResult;
import com.scvh.apps.service.TimeSource;

import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

@Component
public class ResultProvider {

    private TwitterService twitterService;
    private TimeSource timeSource;
    private ResultConverter converter;

    @Inject
    public ResultProvider(TwitterService twitterService, TimeSource timeSource, ResultConverter
            converter) {
        this.twitterService = twitterService;
        this.timeSource = timeSource;
        this.converter = converter;
    }


    public Observable<StructuredResult> searchForTag(String hashtag) {
        Twitter twitter = twitterService.getTwitter();
        List<Tweet> tweets = new ArrayList<>();
        long minId = twitter.searchOperations().search("#" + hashtag + " until:" + timeSource
                .getMinDate(), 1).getSearchMetadata().getMaxId();
        long maxId = minId + 100;
        long topId = twitter.searchOperations().search("#" + hashtag, 1).getSearchMetadata()
                .getMaxId();
        while (maxId >= topId) {
            tweets.addAll(twitterService.getTwitter().searchOperations().search("#" + hashtag,
                    100, minId, maxId).getTweets());
            minId = minId + 100;
            maxId = maxId + 100;
        }
        return converter.convertToJsonResponse(Observable.just(new SearchResult(tweets)),
                timeSource.getMinTime());
    }
}
