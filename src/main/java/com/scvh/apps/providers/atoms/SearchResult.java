package com.scvh.apps.providers.atoms;

import org.springframework.social.twitter.api.Tweet;

import java.util.List;

public class SearchResult {

    private List<Tweet> tweets;

    public SearchResult(List<Tweet> tweets) {
        this.tweets = tweets;
    }

    public List<Tweet> getTweets() {
        return tweets;
    }
}
