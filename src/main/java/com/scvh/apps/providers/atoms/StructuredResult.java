package com.scvh.apps.providers.atoms;


import org.joda.time.Interval;
import org.springframework.social.twitter.api.Tweet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StructuredResult {

    private Map<Interval, List<Tweet>> result;

    public Map<Interval, List<Tweet>> getResult() {
        return result;
    }

    public void putIntoMap(Interval time, Tweet tweet) {
        if (result == null) {
            result = new HashMap<>();
        }
        result.computeIfAbsent(time, k -> new ArrayList<>());
        result.get(time).add(tweet);
    }
}
