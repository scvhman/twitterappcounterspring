package com.scvh.apps.app;

import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class TwitterService {

    private Twitter twitter;

    @Inject
    public TwitterService(Twitter twitter) {
        this.twitter = twitter;
    }

    public Twitter getTwitter() {
        return twitter;
    }

}
